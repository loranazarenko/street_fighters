import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import { openWin } from './services/Utilites';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  static res ={
    name: '',
    attack: 0,
    defense: 0,
    health: 0
    };
    
    async handleFighterClick(event, fighter) {  //
    //this.fightersDetailsMap.set(fighter._id, fighter);
    // console.log('clicked');
    console.log(this.fightersDetailsMap.get(fighter._id));
    if(this.fightersDetailsMap.get(fighter._id)===undefined)
    { 
    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  
      let fighterV = await fighterService.getFighterDetails(fighter._id);  //JSON.parse()
      console.log(fighterV.name);
      FightersView.res.name    = fighterV.name;
      FightersView.res.attack  = fighterV.attack;
      FightersView.res.defense = fighterV.defense;
      FightersView.res.health  = fighterV.health;

      console.log(this.fightersDetailsMap.get(fighterV._id));

      this.fightersDetailsMap.set(fighterV._id, FightersView.res);
      console.log(this.fightersDetailsMap);
      ///console.log(this.fightersDetailsMap.get("health"));
    }
    else{
      console.log(this.fightersDetailsMap.get(fighter._id));
      console.log(this.fightersDetailsMap.get(fighter._id).name);
      console.log(this.fightersDetailsMap.get(fighter._id).attack);
      console.log(this.fightersDetailsMap.get(fighter._id).defense);
      console.log(this.fightersDetailsMap.get(fighter._id).health);
      FightersView.res.name    = this.fightersDetailsMap.get(fighter._id).name;
      FightersView.res.attack  = this.fightersDetailsMap.get(fighter._id).attack;
      FightersView.res.defense = this.fightersDetailsMap.get(fighter._id).defense;
      FightersView.res.health  = this.fightersDetailsMap.get(fighter._id).health;
    }
    openWin(fighter._id,FightersView.res);

  }
}

export default FightersView;