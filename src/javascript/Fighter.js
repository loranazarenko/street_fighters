class Fighter
{
    /** name of the warrior */
    name;
    /** amount of warrior's life */
    attack;
    /** minimal damaging for the enemy's */
    defense;
    /** maximal damaging for the enemy's */
    health;
     
    /** 
    * The constructor creates warrior in the game
    * @param vName - name of the warrior
    * @param vHP  - initial amount of warrior's life
    * @param vMinDmg - minimal damaging for the enemy's 
    * @param vMaxDmg  - maximal damaging for the enemy's
    */
     Fighter(name, attack, defense, health)
    {
        this.name    = name;
        this.attack  = attack;
        this.defense = defense;
        this.health  = health;
    }
    
    getHitPower()
    {
        let criticalHitChance = randomInteger(1,2);
        return power = this.attack * criticalHitChance;
    }
    
    getBlockPower()
    {
        let dodgeChance  = randomInteger(1,2);
        return power = this.defense * dodgeChance ;
    }
     
 }
 function randomInteger(min, max) {
        var rand = min - 0.5 + Math.random() * (max - min + 1)
        rand = Math.round(rand);
        return rand;
      }